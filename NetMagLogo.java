/*
 *  Pseudo-Animated 'NetMag' logo. This logo was used in
 *  magazine NetMag (http://netmag.cz/) in numbers from 35/97 to about 12/98
 *  Copyright (C) 1998 Radim Kolar <hsn@cybermail.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA or
 *  download copy from http://www.gnu.org/copyleft/gpl.html
 *
 */


/* Pseudo - Animovane NetMag Logo */

/* Historie zmen: */

/* 28. 08. 97. Opraveno vertikalni nastavovani pisma */
/* 27. 08. 97. Dodelany reklamni kecy */
/* 21. 08. 97. Dodelany hlasky loading a load error */
/*               zmeneny jmena gifacu na velke pismena */

import java.applet.Applet;
import java.awt.*;

public class NetMagLogo extends java.applet.Applet implements Runnable
{
  Thread runner = null;

  Image    image,logo,bg;
  Graphics draw;
  MediaTracker tracker;
  int w,h;
  

  public String getAppletInfo()
  {
   return "Pseudo-Animated NetMag logo\n(C) Copyright by Radim Kolar (hsn@cybermail.net).\nTFree software under GPL copyleft.";
  }  
  public void init()
  {
   /* inicializace offscreen bufferu */
   image=createImage(size().width, size().height);
   draw=image.getGraphics();
   /* nakreslime si pozadi */
   draw.setColor(Color.orange);    /* oranzova */
   draw.fillRect(0, 0, size().width, size().height);
   tracker=new MediaTracker(this);
   /* load netmag Logo */
   logo=getImage(getCodeBase(),"NETMAG.GIF");
   tracker.addImage(logo,0);
   /* load netmag background */
   bg=getImage(getCodeBase(),"NEWSKY.GIF");
   tracker.addImage(bg,1);
   
   
   w=size().width;
   h=size().height;
  }   
private void logo()
{
   draw.drawImage(logo,0,0,this);
   repaint();
} 
 public void paint(Graphics g)
	{
		update(g);
	}
        
private void pauza(int sec)
{
	 try
			{
				Thread.sleep(sec*1000);
			}
	 catch (InterruptedException e)
	   { }

}
 public void run()
 {
	/* loading ...*/
	draw.setColor(Color.black);  
	draw.setFont(new Font("Helvetica",Font.PLAIN,15));
	draw.drawString("Loading ...",w/2,h/2);
        draw.drawString("Copyright by Radim Kolar 1997",w/3,h/3);
	repaint();
	
   try {
		//Start downloading the images. Wait until they're loaded.
		tracker.waitForAll();
		} catch (InterruptedException e) {}
   if (tracker.isErrorAny()==true) {
	  				       draw.setColor(Color.red);   
									   draw.fillRect(0, 0, w, h);
	                               draw.setColor(Color.black);  
									   draw.drawString("Load error. Sorry, but you need"+
									   " restart your browser for reloading this applet.",0,h/2);
									   repaint();
									   runner=null;
									   return;
								   };    

   while(true)
   {   
   titleimage(bg);
   repaint();
   
   /* anim 1 - prijezd */
   double step=18.0;
   double f=step/w;
   for (int i=w;i>0;i=i-(int)step)
   {
	 draw.drawImage(logo,i,0,this);
	 repaint(i,0,w-i,h);
	 try
			{
				Thread.sleep(40);
			}
	 catch (InterruptedException e)
	   { }
   step=3+(double)i*f;
   }    
  
   logo();   
   pauza(20);
   zobraznapis(0,"NetMag","Alternativni sitovy magazin");
   pauza(25); 
   
   logo();
   pauza(15);
   
   zobraznapis(0,"Chcete se na neco zeptat ?","Prostudujte si nejdrive FAQ!");
   pauza(25); 
   
   logo();
   pauza(15);
   
   zobraznapis(0,"Nove clanky a novi prispevovatele","jsou VZDY vitani ! ");
   pauza(25); 

   logo();
   pauza(15);
   
   zobraznapis(0,"NetMag muze byt vsude, ","mame offline vydani !");
   pauza(25); 

   logo();
   pauza(15);
		 
   zobraznapis(0,"Vsechna dosavadni cisla","jsou zde k dispozici");
   pauza(25); 

   logo();
   pauza(15);
   
   zobraznapis(0,"NetMag","http://netmag.home.cz/");
   pauza(25); 
   
   logo();
   pauza(15);
   
   zobraznapis(0,"Nenechte si ujit","Velkou tabulku ceskeho peeringu");
   pauza(25); 
   
   logo();
   pauza(15);

   zobraznapis(0,"VZDY uvitame nazory nasich","ctenaru. Napiste nam.");
   pauza(25); 
   
   logo();
   pauza(15);

   zobraznapis(0,"NetMag Daily","cerstve informace ze site");
   pauza(25); 

   logo();
   pauza(15);

   zobraznapis(0,"NetMag Consulting","konzultacni sluzby NetMagu");
   pauza(25); 

   logo();
   pauza(15);
   
   zobraznapis(0,"NetMag za vami","prijde i emailem");
   pauza(25); 
	  
   logo();
   pauza(15);

   zobraznapis(0,"Chcete si nechat napsat Java applet","na zakazku ? mailto:hsn@cybermail.net");
   pauza(25); 

   logo();
   pauza(15);

   zobraznapis(0,"Uz jste hlasovali","v nasi miniankete ?");
   pauza(25); 
   
   logo();
   pauza(15);
	  
   zobraznapis(0,"Tak zase zitra","C/");
   pauza(25); 
			   
   /* anim c.2 - odjezd */
   step=23.0;
   f=step/w;

   for (int i=0;i>-w;i=i-(int)step)
   {
	 draw.drawImage(logo,i,0,this);
	 repaint(i,0,w-i,h);
	 try
			{
				Thread.sleep(60);
			}
	 catch (InterruptedException e)
	   { }
   step=3-(double)i*f;
   }    
   pauza(25);
   } /* while */
 }  
	public void start()
	{
		// user visits the page, create a new thread

		if (runner == null)
		{
			runner = new Thread(this);
			runner.start();
		}
	}
	public void stop()
	{
		// user leaves the page, stop the thread

		if (runner != null && runner.isAlive())
			runner.stop();

		runner = null;
	}
private void titleimage(Image src)
 {
  int sw=src.getWidth(this);
  int sh=src.getHeight(this);
  
  for (int i=0;i<w;i=i+sw)
   {
   for (int j=0;j<h;j=j+sh)
	 {
	  draw.drawImage(src,i,j,this);
	 } 
   };
  return;
 } 
  public void update(Graphics g)
	{ 
		 g.drawImage(image, 0, 0, this);
	}
private void zobraznapis(int velikost, String S1, String S2)
{
	titleimage(bg);
	int i;
	Font f;
	FontMetrics fm;
	if (velikost==0) {
	for(i=100;true;i--)
	{
	  f=new Font("Times",Font.PLAIN,i);
	  fm = getFontMetrics(f);
	  if(fm.stringWidth(S1)>w) continue;
	  if(fm.stringWidth(S2)>w) continue;
	  if(2*fm.getHeight()>h) continue;
	  break;    
	} }
	else
	{
	f=new Font("Times",Font.PLAIN,velikost);
	fm = getFontMetrics(f);
	}
	velikost=fm.getHeight();
	draw.setFont(f);
	int zbytek=(h-velikost*2-fm.getLeading())/3;
	draw.drawString(S2,(w-fm.stringWidth(S2))/2,2*velikost+2*zbytek-fm.getLeading()-fm.getDescent());
	draw.drawString(S1,(w-fm.stringWidth(S1))/2,velikost+zbytek-fm.getLeading()-fm.getDescent());
	repaint();   
} 
}